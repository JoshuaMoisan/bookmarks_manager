# Bookmark Management Application

This bookmark management application allows you to save and organize your favorite bookmarks. Built with [React](reactjs.org) and [TypeScript](www.typescriptlang.org), bootstrapped with [Vite](vitejs.dev).

## Prerequisites

Before getting started, make sure you have the following installed on your machine:

- [Node.js](https://nodejs.org)
- [npm](https://www.npmjs.com)

## Installation

1. Clone this code repository to your machine:

```bash
git clone <repository_url>
```

2. Navigate to the project directory:

```bash
cd bookmarks-app
````

3. Install the necessary dependencies using npm:

```bash
npm install
````

## Running the application

1. Start the application in development mode:

```bash
npm run dev
````

2. Open your web browser and visit http://localhost:3000 to access the application

## Usage

1. Enter a URL in the input field and click the "+" button to add a bookmark
2. View and manage your saved bookmarks in the list