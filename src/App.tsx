import { useEffect, useState } from 'react';

import { Form, List, Preview } from './Components';
import { BookmarkState, ResponseState } from './Types';

import './App.css';

function App() {

  const [items, setItems] = useState<BookmarkState[]>([]);
  const [newItem, setNewItem] = useState<string>('');
  const [selectedItem, setSelectedItem] = useState<BookmarkState>();

  useEffect(() => {
    if (newItem) {
      fetchData(newItem)
        .then((resp) => {
          if (resp) {
            handleFetchResponse(resp)
          }
        })
        .catch((error) => { alert(error) })
    }
  }, [newItem])

  async function fetchData(url: string): Promise<ResponseState | undefined> {
    try {
      const response = await fetch(`https://noembed.com/embed?url=${url}`);
      const responseData = await response.json() as ResponseState;
      return (responseData);
    } catch (error) {
      console.error('Erreur lors de la récupération des données :', error);
    }
  }

  const handleFetchResponse = (resp: ResponseState) => {
    const response: BookmarkState = {
      html: resp.html,
      url: resp.url,
      title: resp.title,
      thumbnail_url: resp.thumbnail_url,
      author_name: resp.author_name,
      author_url: resp.author_url,
      upload_date: resp.upload_date,
      duration: resp.duration,
      width: resp.width,
      height: resp.height,
      provider_name: resp.provider_name,
      provider_url: resp.provider_url,
      added_date: new Date(),
    };
    setItems((items) => [...items, response]);
  }

  const handleAdd = (url: string) => {
    setNewItem(url)
  }

  const handleSelect = (item: BookmarkState) => {
    setSelectedItem(item)
  }

  const handleExit = () => {
    setSelectedItem(undefined)
  }

  const handleDelete = (item: BookmarkState) => {
    const updatedItems = [...items]
    updatedItems.splice(items.findIndex((element) => element == item), 1);
    setItems(updatedItems);
    if (selectedItem == item)
      setSelectedItem(undefined)
  }

  return (
    <div className='root-container'>
      <Form onAdd={handleAdd} />
      <div className='root-content'>
        <List items={items} onDelete={handleDelete} onSelect={handleSelect} />
        <Preview item={selectedItem} onExit={handleExit} />
      </div>
    </div>
  )
}

export default App;