import React, { useState } from 'react';

import './Form.css';

interface FormProps {
    onAdd: (url: string) => void;
}

const Form: React.FC<FormProps> = ({ onAdd }) => {
    const [input, setInput] = useState<string>('');

    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setInput(event.target.value);
    };

    const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();
        onAdd(input);
        setInput('');
    };

    return (
        <div className='form-container'>
            <input
                type="text"
                className="form-input"
                value={input}
                placeholder='Bookmark url...'
                onChange={handleInputChange}
            />
            <button
                className="form-button"
                onClick={handleClick}>
                +
            </button>
        </div>
    );
};

export default Form;