import React from 'react';

import { BookmarkState } from '../Types';

import './List.css';

interface ListProps {
    items: Array<BookmarkState>;
    onSelect: (item: BookmarkState) => void;
    onDelete: (item: BookmarkState) => void;
}

const List: React.FC<ListProps> = ({ items, onSelect, onDelete }) => {

    const handleSelect = (event: React.MouseEvent<HTMLLIElement>, item: BookmarkState) => {
        event.preventDefault();
        onSelect(item);
    };

    const handleDelete = (event: React.MouseEvent<HTMLButtonElement>, item: BookmarkState) => {
        event.preventDefault();
        onDelete(item);
    };

    return (
        <div className='list-container'>
            <ul>
                {
                    items.length > 0 ?
                        items.map((item, index) => (
                            <li key={index} className='list-item' onClick={(e) => handleSelect(e, item)}>
                                <div>
                                    <div>{item.title}</div>
                                    <div>{item.url}</div>
                                </div>
                                <button
                                    className="list-button"
                                    onClick={(e) => handleDelete(e, item)}>
                                    x
                                </button>
                            </li>
                        ))
                        :
                        <li key={0} className='list-item'>
                            <div>Aucun bookmark pour le moment</div>
                        </li>
                }
            </ul>
        </div>
    );
};

export default List;