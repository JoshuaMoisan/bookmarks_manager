import React, { useEffect, useState } from 'react';

import { BookmarkState } from '../Types';

import './Preview.css';

interface PreviewProps {
    item: BookmarkState | undefined;
    onExit: () => void;
}

const Preview: React.FC<PreviewProps> = ({ item, onExit }): React.ReactElement => {

    const [uploadedTime, setUploadedTime] = useState<string>('')

    const getUploadGap = (item: BookmarkState) => {
        const now = new Date();
        const differenceMs = now.getTime() - item.added_date.getTime();
        const differenceMn = Math.round(differenceMs / (1000 * 60));

        if (differenceMn < 60) {
            setUploadedTime(`Ajouté il y a ${differenceMn} minute(s)`);
        } else {
            const differenceH = Math.floor(differenceMn / 60);
            const restMn = differenceMn % 60;
            setUploadedTime(`Ajouté il y a ${differenceH} heure(s) et ${restMn} minute(s)`);
        }
    }

    useEffect(() => {
        if (item) {
            getUploadGap(item);
        }
    }, [item])

    const handleExit = (event: React.MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();
        onExit();
    };

    return (
        <div className='preview-container'>
            {item ?
                <>
                    <button
                        className="preview-button"
                        onClick={handleExit}>
                        x
                    </button>
                    <h1>{item.title}</h1>
                    {item.thumbnail_url ? <img src={item.thumbnail_url} /> : <p>Aucun aperçu disponible</p>}
                    <a href={item.url}>{item.url}</a>
                    <p>Auteur : <a href={item.author_url}>{item.author_name}</a></p>
                    {item.upload_date ? <p>Ajouté sur <a href={item.provider_url}>{item.provider_name}</a> le {new Date(item.upload_date).toLocaleDateString('fr-FR', { year: 'numeric', month: 'long', day: 'numeric' })}</p> : null}
                    {item.duration ? <p>Durée : {new Date(item.duration * 1000).toISOString().slice(11, 19)}</p> : null}
                    {item.provider_name == 'Flickr' ? <p>{item.width}px x {item.height}px</p> : null}
                    <p>{uploadedTime}</p>
                </>
                :
                <p>Aucun bookmark sélectionné</p>
            }
        </div>
    );
};

export default Preview;