export interface BookmarkState {
    html: string;
    url: string;
    title: string;
    thumbnail_url: string;
    author_name: string;
    author_url: string;
    upload_date?: Date;
    duration?: number;
    width: number;
    height: number;
    provider_name: string;
    provider_url: string;
    added_date: Date;
}